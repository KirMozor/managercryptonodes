using System;
using Client.Core;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
namespace Client
{
    public class Program
    {
        public static string token = "";
        public static void Main(string[] args)
        {
            Token tokenObj = new Token();

            try { tokenObj.ReadToken("appsettings.json"); }
            catch (NullReferenceException)
            {
                token = tokenObj.GenerateToken();
                tokenObj.WriteToken(token, "appsettings.json");
                Console.WriteLine($"Your token: {token}");
            }

            token = tokenObj.ReadToken("appsettings.json");
            CreateHostBuilder(args).Build().Run();
        }

        private static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
