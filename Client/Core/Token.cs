using System;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Client.Core;

public class Token
{
    /// <summary>
    /// This method generates a token
    /// <example>
    /// For example:
    /// <code>
    /// Token tokenObj = new Token();
    /// string token = tokenObj.GenerateToken();
    /// </code>
    /// </example>
    /// </summary>
    public string GenerateToken()
    {
        Guid myToken = Guid.NewGuid();
        return myToken.ToString();
    }

    /// <summary>
    /// This method reads the token from the configuration file
    /// <example>
    /// For example:
    /// <code>
    /// Token tokenObj = new Token();
    /// string token = tokenObj.ReadToken("appsettings.json");
    /// </code>
    /// </example>
    /// </summary>
    public string ReadToken(string path)
    {
        using (StreamReader reader = new StreamReader(path))
        {
            JObject fileJson =
                JsonConvert.DeserializeObject<JObject>(reader.ReadToEnd());
            return fileJson["Token"].ToString();
        }
    }

    /// <summary>
    /// This method writes a token to the configuration file
    /// <example>
    /// For example:
    /// <code>
    /// Token tokenObj = new Token();
    /// tokenObj.WriteToken(token, "appsettings.json");
    /// </code>
    /// </example>
    /// </summary>
    public async void WriteToken(string token, string path)
    {
        using (StreamWriter writer = new StreamWriter(path, false))
        {
            await writer.WriteLineAsync($"{{\n  \"Logging\": {{\n    \"LogLevel\": {{\n      \"Default\": \"Information\",\n      \"Microsoft\": \"Warning\",\n      \"Microsoft.Hosting.Lifetime\": \"Information\"\n    }}\n  }},\n  \"AllowedHosts\": \"*\",\n  \"Token\": \"{token}\"\n}}");
        }
    }
}