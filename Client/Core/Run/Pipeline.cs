using Shell.NET;

namespace Client.Core.Run;

public class Pipeline
{
    /// <summary>
    /// This method runs the command in Bash and returns the output
    /// <example>
    /// For example:
    /// <code>
    /// Pipeline pipeline = new Pipeline();
    /// string result = pipeline.runCommand("echo 'Cryptocurrency top!');
    /// </code>
    /// Cryptocurrency top!
    /// </example>
    /// </summary>
    public string runCommand(string command)
    {
        Bash shell = new Bash();
        return shell.Command(command).Output;
    }
}
