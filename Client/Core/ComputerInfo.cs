using System;
using System.Diagnostics;
using System.IO;
using Client.Core.Run;

namespace Client.Core;

public class ComputerInfo
{
    Pipeline _pipeline = new Pipeline();

    /// <summary>
    /// Get the current CPU load as a percentage.
    /// </summary>
    /// <returns>Returns the current CPU load as a percentage</returns>
    public string cpuUpload()
    {
        string output = _pipeline.runCommand("cat /proc/loadavg");
        return output.Split(' ')[0];
    }

    /// <summary>
    /// Get the current amount of free memory in megabytes
    /// </summary>
    /// <returns>Returns the current amount of free memory in KB</returns>
    public float freeRam()
    {
        string output = _pipeline.runCommand("free | awk '/Mem:/ {print $4} /Swap:/ {print $4}'");
        string[] lines = output.Split(Environment.NewLine);
        long freeRam = Convert.ToInt64(lines[0]);
        long freeSwap = Convert.ToInt64(lines[1]);
        return freeRam + freeSwap;
    }

    /// <summary>
    /// Get the current disk load in percent
    /// </summary>
    /// <returns>Returns the current disk load in percent</returns>
    public string dickUpload()
    {
        string output = _pipeline.runCommand("df -h / | awk '{print $5}'");
        string[] outputSplit = output.TrimEnd('%').Split("\n");
        return outputSplit[1];
    }

    /// <summary>
    /// Get the computer name of the Linux system.
    /// </summary>
    /// <returns>Returns the Linux computer name</returns>
    public string nameComputer()
    {
        return File.ReadAllText("/sys/class/dmi/id/product_name").Trim();
    }
}