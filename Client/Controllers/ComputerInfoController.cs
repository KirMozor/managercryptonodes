using Client.Core;
using Microsoft.AspNetCore.Mvc;

namespace Client.Controllers;

/// <summary>
/// The SystemMonitorController class provides methods to retrieve system performance metrics such as CPU load, available RAM, disk load and computer name on a Linux system.
/// </summary>
[Route("api/system")]
[ApiController]
public class ComputerInfoController : ControllerBase
{
    private ComputerInfo _computerInfo = new ComputerInfo();
    
    /// <summary>
    /// Get the current CPU load as a percentage.
    /// </summary>
    /// <returns>Returns the current CPU load as a percentage in an OK HTTP response</returns>
    [HttpGet]
    [Route("cpu-upload")]
    public IActionResult GetCpuLoad()
    {
        if (Request.Headers["Authorization"] == Program.token) 
        { return Ok(_computerInfo.cpuUpload()); }
        { return Forbid("{ \"Result\": \"Forbidden\" }"); }
    }
    
    /// <summary>
    /// Get the current amount of free memory in megabytes
    /// </summary>
    /// <returns>Returns the current amount of free memory in megabytes in an OK HTTP response</returns>
    [HttpGet]
    [Route("free-memory")]
    public IActionResult GetFreeMemory()
    {
        if (Request.Headers["Authorization"] == Program.token) 
        { return Ok(_computerInfo.freeRam()); }
        { return Forbid("{ \"Result\": \"Forbidden\" }"); }
    }
    
    /// <summary>
    /// Get the current disk load in percent
    /// </summary>
    /// <returns>Returns the current disk load in percent in an OK HTTP response</returns>
    [HttpGet]
    [Route("disk-upload")]
    public IActionResult GetDiskLoad()
    {
        if (Request.Headers["Authorization"] == Program.token)
        { return Ok(_computerInfo.dickUpload()); }
        { return Forbid("{ \"Result\": \"Forbidden\" }"); }
    }

    /// <summary>
    /// Get the computer name of the Linux system.
    /// </summary>
    /// <returns>Returns the Linux computer name and OK HTTP response</returns>
    [HttpGet]
    [Route("computer-name")]
    public IActionResult GetNameComputer()
    {
        if (Request.Headers["Authorization"] == Program.token)
        { return Ok(_computerInfo.nameComputer()); }
        { return Forbid("{ \"Result\": \"Forbidden\" }"); }
    }
}