using Microsoft.AspNetCore.Mvc;
using System.Net;
using Client.Core.Run;

namespace Client.Controllers.Run
{
    [Route("api/run")]
    [ApiController]
    public class PipelineController : ControllerBase
    {
        /// <summary>
        /// This method is needed to run the command on a node
        /// </summary>
        /// <returns>Returns the result of the command</returns>
        [HttpGet]
        [Route("command")]
        public IActionResult RunCommand(string command)
        {
            if (Request.Headers["Authorization"] == Program.token)
            {
                Pipeline pipeline = new Pipeline();
                string resultCommand = pipeline.runCommand(command);

                return Ok($"{{\n  \"Result\": \"Success\",\n  \"Return\": \"{resultCommand}\"\n}}");   
            }
            { return Forbid("{ \"Result\": \"Forbidden\" }"); }
        }
    }
}