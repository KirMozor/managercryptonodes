using System;
using Microsoft.AspNetCore.Mvc;
using System.Net;

namespace Client.Controllers
{
    [Route("api")]
    [ApiController]
    public class OtherController : ControllerBase
    {
        /// <summary>
        /// This method pings the nodes
        /// </summary>
        /// <returns>{ "Result": "Success" }</returns>
        [HttpGet]
        [Route("ping")]
        public IActionResult PingNodes()
        {
            if (Request.Headers["Authorization"] == Program.token)
            { return Ok("{ \"Result\": \"Success\" }"); }
            { return Forbid("{ \"Result\": \"Forbidden\" }"); }
        }
    }
}