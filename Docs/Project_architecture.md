# Project architecture

## Node - Server
![Project_architecture_0](https://gitlab.com/KirMozor/CryptoNodesManager/-/raw/main/Docs/Images/Project_architecture_0.png)

The project consists of a classic Client - Server scheme (in our case Node - Server)
In the picture above you can see that the server as well as the node is written in ASP.NET, the node communicates with the server using Web API which is also written in ASP.NET

## Why is there so little written?
This Markdown document will be updated as the project is written :)