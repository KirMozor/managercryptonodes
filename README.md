# CryptoNodesManager
![Developing?](https://img.shields.io/static/v1?label=Developing?&message=Yes&color=green)
![Made with ASP.NET](https://img.shields.io/badge/Made%20with-ASP.NET-1f425f.svg)
![Linux](https://img.shields.io/badge/-Linux-grey?logo=linux)
![License](https://img.shields.io/gitlab/license/Xeronix/CryptoNodesManager)

## Description

This project will facilitate your work with crypto nodes. It allows you to install the desired node by one click on the button, automatically update the crypto node and backup critical data of the node (e.g. crypto wallet which is tied to the node)

## Getting started

### Installation on a server
The server hosting the site must have the following characteristics in order for it to work:

**OS:**
- Ubuntu
- Debian

**CPU:**
- 4 core
- 2.1 GHz

**RAM:**
- 2 Gb

**Disk space:**
- 500 Mb

Before working with the program, update the server. You should also have `git` installed on your server; if not, here is the command: 
- Install git:
    - `sudo apt install git`
- Cloning repository:
    - `git clone https://gitlab.com/Xeronix/CryptoNodesManager.git`
- Run installation script:
    - `cd CryptoNodesManager && chmod +X ./install.sh && ./install.sh --server`

Wait until the installation is complete and then go to the address that the installer gave you, congratulations, you have installed the server!

### Installation on a node
For the server to communicate with your future nodes you need to install the client on your node, don't be afraid, it's easy!
In essence, you need to do the same thing as in the first point of the installation, only run a different script:
- Install git:
    - `sudo apt install git`
- Cloning repository:
    - `git clone https://gitlab.com/Xeronix/CryptoNodesManager.git`
- Run installation script:
    - `cd CryptoNodesManager && chmod +X ./install.sh && ./install.sh --client`

Wait for the installation to finish, congratulations, you have installed the client for the node! Copy the address the script gave you and paste it into the server site at the path: null

### Documentation project
You can find the project documentation [here](https://gitlab.com/Xeronix/CryptoNodesManager/-/blob/main/Docs/Project_architecture.md)

## Authors and acknowledgment
#### Main author: KirMozor
- Telegram: https://t.me/+-QPSC9v5pNthNTVi
- GitLab: https://gitlab.com/KirMozor

## License
Apache license
